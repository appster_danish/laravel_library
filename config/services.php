<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    
    'appster' => [
        'userModel'=> App\User::class,
        'userDeviceModel' => App\UserDevices::class,
        'isVarificaitonMailEnable' => 0,
        'newUserRole' => 0,
        'isTempPass' => 0,
        'activeStatus' => 1,
        'notActive' => 0,
        'notAutherized' => 2,
        'userRole' =>2,
        'adminRole' => 1,
        'randomPasswordLength' => 10,
        'randomReferralCodeLength' => 8,
    ]

];
