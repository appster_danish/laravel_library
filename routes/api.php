<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', function (Request $request) {
    
    
    return $request->user();
})->middleware('auth:api');


Route::group(['prefix' => 'basic'], function () {
    // Resource Handaling
    Route::post('login', 'Api\User\UserController@login');
    Route::post('socialLogin', 'Api\User\UserController@socialLogin');
    Route::post('signUp', 'Api\User\UserController@signUp');
    Route::post('forgotPassword', 'Api\User\UserController@forgotPassword');
    Route::post('changePassword', 'Api\User\UserController@changePassword');
    Route::delete('signOut', 'Api\User\UserController@deleteSignOut');
 });