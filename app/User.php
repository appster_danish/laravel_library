<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Appster\Basic\UserBasic;




class User extends Authenticatable
{
    use Notifiable;
    use UserBasic;
    
     protected $table = 'tbl_user';
    protected $primaryKey = 'id';
    
    protected $fillable = [];
    protected $appends = [];
    
  

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'password', 'lastLogin'];
    
    

}
