<?php
namespace App\Helpers\Utility;

use Illuminate\Support\Facades\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api;

class ValidateJson extends Controller {

    function __construct() {
        $request = Request::instance();
        $this->jsondata = $request->getContent();
        
        
        $validateJson = $this->jsonValidate($this->jsondata);

        if ($validateJson !== true) {

            $this->renderJson(" ", 'error', $validateJson);
        }
        $this->postData = json_decode($this->jsondata, true);
    }
            
    public function jsonValidate($json, $assoc_array = FALSE) { 

        // decode the JSON data
        json_decode($json, $assoc_array);

        // switch and check possible JSON errors
        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                $error = ''; // JSON is valid
                break;
            case JSON_ERROR_DEPTH:
                $error = 'Maximum stack depth exceeded.';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                $error = 'Underflow or the modes mismatch.';
                break;
            case JSON_ERROR_CTRL_CHAR:
                $error = 'Unexpected control character found.';
                break;
            case JSON_ERROR_SYNTAX:
                $error = 'Syntax error, malformed JSON.'; 
                break;
            // only PHP 5.3+
            case JSON_ERROR_UTF8:
                $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                break;
            default:
                $error = 'Unknown JSON error occured.';
                break;
        }



        // everything is OK
        return true;
    }

    static function renderJson($status, $message, $response_data) {
        $data['status'] = $status;
        $data['message'] = $message;
        $data['data'] = $response_data; 
       
        echo json_encode($data, JSON_FORCE_OBJECT);
        
    }

    function jsonValidater() {
        return $this->postData;
    }

}
