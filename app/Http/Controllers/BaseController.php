<?php

namespace App\Http\Controllers;



class BaseController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        
    }

    public function sendJsonResponse($response) {
        return \Illuminate\Support\Facades\Response::json($this->convertToCamelCase($response), $response['status_code'])->header('Content-Type', "application/json");
    }

    /**
     * Convert to Camel Case
     *
     * Converts array keys to camelCase, recursively.
     * @param  array  $array Original array
     * @return array
     */
    protected function convertToCamelCase($array) {

        $converted_array = [];
        foreach ($array as $old_key => $value) {
            if (is_array($value)) {
                $value = $this->convertToCamelCase($value);
            } else if (is_object($value)) {
                if (($value instanceof Model) || method_exists($value, 'toArray')) {
                    $value = $value->toArray();
                } else {
                    $value = (array) $value;
                }


                $value = $this->convertToCamelCase($value);
            }
            $converted_array[camel_case($old_key)] = $value;
        }

        return $converted_array;
    }

    public function getData() {
        $this->postData = new \App\Helpers\Utility\ValidateJson();
        $this->request = $this->xssClean($this->postData->jsonValidater());
    }

    protected function xssClean($array) {

        $converted_array = [];
        if (is_array($array)) {
            foreach ($array as $old_key => $value) {
                if (is_array($value)) {
                    $value = $this->xssClean($value);
                } else if (is_object($value)) {
                    if (($value instanceof Model) || method_exists($value, 'toArray')) {
                        $value = $value->toArray();
                    } else {
                        $value = (array) $value;
                    }


                    $value = $this->xssClean($value);
                }
                if (!is_array($value)) {
                    $value = htmlspecialchars($value);
                }
                $converted_array[$old_key] = $value;
            }
        }

        return $converted_array;
    }

}
