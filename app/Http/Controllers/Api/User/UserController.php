<?php
namespace App\Http\Controllers\Api\User;

use Illuminate\Http\Request;

use Appster\Basic\Utility\ResponseFormatter;
use App\Http\Controllers\BaseController;
use App\User;

class UserController extends BaseController {

    protected $message;
    protected $user;
    protected $request;

    public function __construct(User $user) {
      $this->middleware('App\Http\Middleware\ApiAuth', ['except' => ['signUp', 'verification','changePassword', 'deleteSignOut', 'socialLogin', 'forgotPassword', 'login']]);
       
        $this->user = $user;
        $this->response = new ResponseFormatter();
        
    }

    /**
     * 
     * New User Registration
     */
    public function signUp(\App\Http\Requests\UserRequests\SignupRequest $request) { 
        $this->getData();
      
        $response = $this->user->signUp($this->request); 
        
        
        return $this->sendJsonResponse($response);
    }

    /**
     * 
     * User Login
     */
    public function login(\App\Http\Requests\UserRequests\LoginRequest $request) {
        $this->getData();
        $response = $this->user->login($this->request);
        return $this->sendJsonResponse($response);
    }
    /**
     * 
     * @param socialLogin $request json
     * @return json
     */
    public function socialLogin(\App\Http\Requests\UserRequests\socialLogin $request) {
        $this->getData();
        $response = $this->user->socialLogin($this->request);
        return $this->sendJsonResponse($response);
    }
    
    /**
     * 
     * User Change Password
     */
    public function changePassword(\App\Http\Requests\UserRequests\ChangePasswordRequest $request) {
        $this->getData();
       
        $user_id = $this->request['loggedUserId'];
        $old_password = $this->request['oldPassword'];
        $new_password = $this->request['newPassword'];

        $response = $this->user->changePassword($user_id, $old_password, $new_password);
        return $this->sendJsonResponse($response);
    }

    /**
     * 
     * User Forget Password
     */
    public function forgotPassword(\App\Http\Requests\UserRequests\ForgotPasswordRequest $request) {
        $this->getData();
        $response = $this->user->forgotPassword($this->request['email']);
        return $this->sendJsonResponse($response);
    }

    /**
     * 
     * User Logout                        
     */
    public function deleteSignOut(Request $request) {
        $accessToken = $request->header('accessToken');
     
        $response = $this->user->logout( $accessToken);
        return $this->sendJsonResponse($response);
    }


    
    /*
     * User Email Varification from email link
     */
     public static function verification($confirmation_code) {
         $user = new User();
        return $user->varification($confirmation_code);
    }
    
    
}
