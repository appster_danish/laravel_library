<?php

namespace App\Http\Requests\UserRequests;
use App\Http\Requests\BaseApiRequest;
class LoginRequest extends BaseApiRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'email' => 'required:email',
            'password' => 'required',
            'deviceInfo' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
