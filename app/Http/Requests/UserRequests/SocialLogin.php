<?php

namespace App\Http\Requests\UserRequests;
use App\Http\Requests\BaseApiRequest;
class socialLogin extends BaseApiRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
             'facebookId' => 'required',
            'email' => 'required',
            'deviceInfo' => 'required',
            'facebookToken' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
