<?php

namespace App\Http\Requests\UserRequests;
use App\Http\Requests\BaseApiRequest;
class SignupRequest extends BaseApiRequest {

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        return [
              'email' => 'email|required|unique:tbl_user',
                'password' => 'required',
                'fullname' => 'required',
                'deviceInfo' => 'required',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
