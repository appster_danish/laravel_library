<?php

namespace App\Http\Requests\UserRequests;
use App\Http\Requests\BaseApiRequest;
use Config;
class UpdateUser extends BaseApiRequest {

    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        $this->postData = new \App\Helpers\Utility\ValidateJson();
        $this->postData->jsonValidater();
        
        
        
        $validate =  array(
            //'roleId' => 'required',
            'fullName' => 'required',
        );
       
     
        return $validate;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

}
