<?php namespace App\Http\Middleware;

use Closure;
use App\UserDevices;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    
    public function handle($request, Closure $next)
    {       
        
        $loggedUserId=$request->header('loggedUserId');
        

             $user = UserDevices::
                     where('access_token', $request->header('accessToken'))
                     ->where('user_id',$loggedUserId)
                    ->first();
     
              
       if(!is_object($user)){
           $response = array(
                        'status' =>  "0",
                        'response_code' => "604",
                        'data' => array(),
                        'message' => 'Invalid user',
			);
			return json_encode($response);
			
       }
    
        return $next($request);
    }
}
