<?php

namespace App;

use Eloquent;

class UserDevices extends Eloquent  {
    
    
    protected $table        = 'tbl_user_device';
    
   
    
    public static function unRegisterAll($accessToken){
        
        UserDevices::where('access_token',$accessToken)->delete();
       return true;
    }
    
//    public function user() {
//        return $this->belongsTo('App\Models\User');
//    }
//    
    
}