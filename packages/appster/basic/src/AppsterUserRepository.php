<?php

namespace Appster\Basic;


use Illuminate\Support\Facades\Config;
use Appster\Basic\Contracts\UserContract as AppsterUserContract;

class AppsterUserRepository implements AppsterUserInterface
{
  
    public function find()
    {
        $user_models = Config::get('services.appster.userModel');

        if (! is_array($user_models)) {
            $user_models = [$user_models];
        }

        foreach ($user_models as $user_model) {
           return  $this->createUserModel($user_model);
        }

    }
    
    
     protected function createUserModel($class)
    {
        $model = new $class;

        if (! $model instanceof AppsterUserContract) {
            throw new \InvalidArgumentException(trans('appster::messages.model_not_found'));
        }

        return $model;
    }
    






    
}
