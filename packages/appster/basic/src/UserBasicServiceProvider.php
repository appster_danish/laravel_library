<?php

namespace Appster\Basic;

use Illuminate\Support\ServiceProvider;

class UserBasicServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot() {
        $service_path = explode("/", __DIR__);
        $service_path[count($service_path) - 1];
        $this->loadViewsFrom(__DIR__.'/views', 'appsterView');
        $this->loadTranslationsFrom(__DIR__.'/Lang/', 'appster');
       
         $this->publishes([
        __DIR__.'/views' => base_path('resources/views/appster/basic'),
        ]);
         
         
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        $this->app->singleton('Appster\Basic\AppsterUserInterface', function () {
            return new AppsterUserRepository;
        });
    }

}
