<?php

namespace Appster\Basic\Utility;

/*
 * This is Utility Class of the Image
 */

class UtilityHelper {

    public static function generateRandomString($passwordLength) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%*&';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $passwordLength; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    /**
     * 
     * @param type $ptime
     * @return string
     */
    public static function timeStamp($ptime) {

        $ptime = strtotime($ptime);
        $etime = time() - $ptime;

        if ($etime <= 0) {
            //return '0 seconds';
            return 'Just now';
        }

        $a = array(12 * 30 * 24 * 60 * 60 => 'y',
            30 * 24 * 60 * 60 => 'M',
            24 * 60 * 60 => 'd',
            60 * 60 => 'hr',
            60 => 'min',
            1 => 's'
        );

        foreach ($a as $secs => $str) {

            $d = $etime / $secs;

            if ($d >= 1) {
                $r = round($d);
                if (in_array($str, array('s'))) {
                    return 'Just now';
                } else if (!in_array($str, array('min', 'hr', 'd'))) {
                    return date('m/d/Y', $ptime);
                } else {
                    //return $r.$str.($r > 1 ? 's' : '') . ' ago';
                    return $r . " " . $str . ' ago';
                }
            }
        }
    }
    
  

}
