<?php

namespace Appster\Basic\Utility;

use Log;
use \Symfony\Component\HttpFoundation\Response;

class ResponseFormatter {
    
    public function responseSuccess($message = '', $data = array()) {
        $code = Response::HTTP_OK; 
        $data = (object) $data;
        $response = $this->responseCommon($data,1, $code, $message);

        return $response;
    }

    public function responseCustom($statusCode, $message = '', $data = array()) {
        $data = (object) $data;
        $response = $this->responseCommon($data,0, $statusCode, $message);
        return $response;
    }

    public function responseUnauthorized($message = '', $data = array()) {
        
        $code = Response::HTTP_UNAUTHORIZED; 
        $data = (object) $data;
        $response = $this->responseCommon($data,0, $code, $message);
        return $response;
    }

    public function responseBadRequest($message = '', $data = array()) {        
        $code = Response::HTTP_BAD_REQUEST; 
        $data = (object) $data;
        $response = $this->responseCommon($data,0, $code, $message);
        return $response;
    }

    public function responseNotFound($message = '', $data = array()) {
        
        $code = Response::HTTP_NOT_FOUND; 
        $data = (object) $data;
        $response = $this->responseCommon($data,0, $code, $message);
        Log::error($response);
        return $response;
    }

    public function responseServerError($message = '', $data = array()) {
        
        $code = Response::HTTP_INTERNAL_SERVER_ERROR; 
        $data = (object) $data;
        $response = $this->responseCommon($data,0, $code, $message);
        Log::error($response);
        Log::error($response);
        
        return $response;
    }

    public function responseCommon($data,$status="", $response="", $message = '') {
        $response = array(
            'status' => $status,
            'status_code' => $response,
            'message' => $message,
            'result' => (object) $data,
        );
        return $response;
    }

}
