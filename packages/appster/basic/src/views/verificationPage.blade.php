<!DOCTYPE html>
<html>
    <head>
        <title>Verification</title>

        <style type="text/css">

            @import url('http://getbootstrap.com/dist/css/bootstrap.css');

            html, body, .container-table {
                height: 100%;
            }
           
            body {
               
                background-position: center center;
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-size: cover;
            }
            .container-table {
                display: table;
            }
            .vertical-center-row {
                display: table-cell;
                vertical-align: top;
                padding-top: 70px;
            }

            @font-face {
                font-family: 'ProximaNova-Bold';
                src: url("{{ asset('admin-assets/fonts/ProximaNova-Bold.otf') }}") format('otf') format('svg');
                font-weight: normal;
                font-style: normal;
            }

           
            
        </style>
    </head>
    <body>


        <div class="container container-table">
            <div class="row vertical-center-row">
                <p style="text-align: center; padding-bottom: 50px;"><img src="{{ asset('admin-assets/images/logo-msg.png') }}" width= "150px"></p>
                <div class="text-center col-md-6 col-md-offset-3" style="font-size: 2em; font-family: 'ProximaNova-Bold'; padding: 2px 2px; display: inline-block; color: rgb(76,87,99); "><?PHP print_r($message); ?></div>
            </div>
        </div>

    </body>


</html>
<!--<b>Congratulations!</b> You have successfully verified your Email.  Now You can login into Example app.-->