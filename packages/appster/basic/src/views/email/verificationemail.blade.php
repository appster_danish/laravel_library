<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title></title>
</head>

<body  style="margin: 0; padding: 0; outline:0 none;">
<table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse; max-width:590px;">
<tr>
<td align="center" style="border-bottom:1px solid #ccc; padding:15px 0 12px; background:#36B3A8; font: 18px/22px arial; color: #fff;">
  <a href=""><img src="" title="Example" height="40"></a>
</td>
</tr>
<tr>
 <td align="left" style=" font-family:arial; line-height:20px;"><div style="border-left:1px solid #f4f4f4; border-right:1px solid #f4f4f4; border-bottom:1px solid #f4f4f4; padding:30px 26px 19px;">
      <h4 style="margin:0 0 15px 0">Hi {{$name}},</h4>
          <p style="color:#20491c; font-weight: normal; margin: 0 0 25px; padding: 0;  font-size: 14px; line-height: 24px;">
              Thank you for registering to Example.
          </p>
           <p style="color:#20491c; font-weight: normal; margin: 0 0 25px; padding: 0;  font-size: 14px; line-height: 24px;">
              Please click below link to verify your Example account.<br>
              <a href="{{url('verify').'/'.$url}}">Click here to verify</a>
          </p>
          <p style="color:#20491c; font-weight: normal; margin:0; padding: 0;  font-size: 14px;">Warm Regards</p>
          <p style="color:#20491c; font-weight: normal; margin: 0px 0 5px; padding: 0;  font-size: 13px;">Example Team</p>
        </div>
        </td>
        </tr>
        <tr>
      <td style="border-bottom:1px solid #ccc; padding:15px 15px 12px; background:#36B3A8; font: 18px/22px arial; color: #fff;">
        <table  width="100%" style="border-collapse: collapse;">
            <tbody>
              <tr>
                            <td align="center">
                              <p style="font: 12px/16px arial">© <span class="date">2016</span> Example, Inc. All rights reserved.</p>
                            </td>
                       </tr>
                    </tbody>
               </table>
    
    
</td>
</tr>
</table>

</body>
</html>
