<?php

return [
    
    'incorrect_email'=>'This email is not registered with us',
    'forgot_pswd_email'=>'An email with the reset password link has been sent to your email id',
    'forgot_password_email'=>'An email with the temporary password has been sent to your email id',
    'logout'=>'Logged out successfully',
    'user_register_success'=>'Registration successful',
    
    'login_success'=>'Login successful',
    'invalid_login_credentials'=>'Invalid credentials',
    'auth_required'=>'Authenitication required',
    'exception_msg' => 'Something went wrong. Please try again later',
    
    'user_created'=>'User created successfully',   
    'user_deleted'=>'User deleted successfully',
   'facebook_login_success' => 'Facebook login successful!', 
    'status_changed' => 'Status changed successfully ',
    'user_blocked' => 'Please contact admin to activate this user !',
    'mail_sent' => 'Mail Sent!', 
    'facebook_mismatch' => 'Facebook does not match!', 
    'facebook_login_fail' => 'Facebook Login Failed!', 
    'success' => 'Success!',
    'unauthorized' => 'Not Authorized!', 
    'credential_mismatched' => 'Credentials doesnt match!',
    'user_not_found_reset_pass'=> 'User not found!',
    'userNotVerified' => 'User not verified!', 
    'EmailVerifyError' => 'Email verification error! ',
    'userBlocked' => 'This user is blocked! ',
    'email_exists' => 'Email already exists !',
    'user_registered' => 'User resistered successfully!',
    'invalid_email_password' => 'Invalid email or password!',
    'invalid_credentials' => 'Invalid credentials!',
    'login_successful' => 'User logged in successfully!',
    'unauthorised' => 'You are not authorised to access from this Site!',
    'update_successful' => "Info has been updated successfully",
    'update_unsuccessful' => 'Info can not be updated.Please try after sometime!',
  
    
    
    
    
    
    'model_not_found' => 'Model does not implement User.',
    'invi_already_accepted' => 'Invitation Already Accepted.',
    'invi_accepted' =>  'Invitation Accepted'
    
    
    
    
   
];
