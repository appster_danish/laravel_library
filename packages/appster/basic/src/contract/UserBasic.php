<?php
namespace Appster\Basic\contract;


interface UserBasic
{
    public function signUp($request);
    public function login($request);
    public function changePassword($userId,$oldPassword,$newPassword);
    public function forgotPassword($email);
    public function socialLogin($request);
    public function logout($accessToken);
    
}

