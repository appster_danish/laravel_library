<?php

namespace Appster\Basic;

use Illuminate\Support\Facades\Config;
use DB;
use Appster\Basic\Utility\ResponseFormatter;
use Appster\Basic\Utility\UtilityHelper;
use Mail;

trait UserBasic {

    protected $user_device_model;
    protected $response;

    /**
     * Write the entity to persistent storage.
     *
     * @return void
     */
    public function saveUserInstance() {
        $this->save();
    }

    /**
     * 
     * @param function : getUserDeviceModel()
     * @returns Description : Get Device Model.
     * 
     */
    public function getUserDeviceModel() {
        $user_models = Config::get('services.appster.userDeviceModel');
        if (strlen($user_models) > 0) {
            return new $user_models;
        }
    }

    /**
     * 
     * @param function : signUp()
     * @returns Description : Here we are registering user.
     * 
     */
    public function signUp($json) {
        $response = new ResponseFormatter();
        try {
            DB::beginTransaction();
            if (!empty($json['referralCode']) && $json['referralCode'] != '') {
                $referred_by = User::referredIdByCode($json['referralCode']);
                if ($referred_by == '') {
                    return $this->response->responseServerError(trans('appster::messages.incorrect_email'));
                }
            }
            // Add User Information
            $user = $this->addUser($json);
            $device_info = $json['deviceInfo'];
            // Store Device Information
            $token = $this->addDevice($user->id, $device_info);
            $user->accessToken = $token;
            $is_varificaiton_mail_enable = Config::get('services.appster.isVarificaitonMailEnable');
            if ($is_varificaiton_mail_enable) {
                $this->sendEmailVerificationLink($user);
            }

            $user->user_id = $user->id;
            $user->role_id = Config::get('services.appster.newUserRole');
            $user->isTempPass = Config::get('services.appster.isTempPass');
            $user->facebook_id = '';
            unset($user->id);
            unset($user->role_id);
            unset($user->verification_code);
            unset($user->remember_token);
            unset($user->referral_code);
            unset($user->referred_by);
            unset($user->referred_count);
            DB::commit();
            return $response->responseSuccess(trans('appster::messages.user_register_success'), $user);
        } catch (Exception $ex) {
            DB::rollBack();
            return $response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : addDevice()
     * @returns Description : insert device in device table.
     * 
     */
    public function addDevice($user_id, $device_info) {
        $user_device_model = $this->getUserDeviceModel();
        if (!empty($user_id)) {
            $user_device_model->where('user_id', $user_id)->delete();
        }
        // if found deviceToken then delete.    
        if (!empty($device_info['deviceToken'])) {
            $user_device_model->where('device_token', $device_info['deviceToken'])->delete();
        }
        // if found uniqueDeviceId then delete.  
        if (!empty($device_info['deviceId'])) {
            $user_device_model->where('unique_device_id', $device_info['deviceId'])->delete();
        }

        $user_device_model = $this->getUserDeviceModel();
        $user_device_model->user_id = $user_id;
        if (!empty($device_info['deviceId'])) {
            $user_device_model->unique_device_id = $device_info['deviceId'];
        }
        if (!empty($device_info['deviceType'])) {
            $user_device_model->device_type = $device_info['deviceType'];
        }
        if (!empty($device_info['deviceToken'])) {
            $user_device_model->device_token = $device_info['deviceToken'];
        }
        $user_device_model->save();
        $user_device_model->access_token = md5($user_device_model->id . $user_device_model->user_id . time());
        $user_device_model->save();
        return $user_device_model->access_token;
    }

    /**
     * 
     * @param function : AddUser()
     * @returns Description : insert user .
     * 
     */
    public function addUser($data) {

        $is_varificaiton_mail_enable = Config::get('services.appster.isVarificaitonMailEnable');
        $status = Config::get('services.appster.activeStatus');
        if ($is_varificaiton_mail_enable) {
            $status = Config::get('services.appster.notActive');
        }
        $verification_code = time();
        $this->name = $data['fullname'];
        $this->email = $data['email'];
        $this->password = bcrypt($data['password']);
        $this->role_id = Config::get('services.appster.userRole');
        $this->phone_number = !empty($data['phoneNumber']) ? $data['phoneNumber'] : '';
        $this->status = $status;
        $this->verification_code = $verification_code;
        $this->saveUserInstance();
        return $this;
    }

    /**
     * 
     * @param function : login()
     * @returns Description : Here we are authenticating user.
     * 
     */
    public function login($json) {
        try {

            $this->response = new ResponseFormatter();
            $device_info = $json['deviceInfo'];
            if (\Auth::attempt(['email' => $json['email'], 'password' => $json['password']])) {
                $user = $this->find(\Auth::user()->id);
                if (is_object($user)) {
                    $is_varificaiton_mail_enable = Config::get('services.appster.isVarificaitonMailEnable');
                    if ($is_varificaiton_mail_enable && $user->status == Config::get('services.appster.notActive')) {
                        $this->sendEmailVerificationLink($user);
                        return $this->response->responseUnauthorized(trans('appster::messages.EmailVerifyError'));
                    }
                    if ($user->status == Config::get('services.appster.notAutherized')) {
                        return $this->response->responseUnauthorized(trans('appster::messages.userBlocked'));
                    }
                    $user->last_login = date('Y-m-d h:i:s');
                    $user->save();
                    $token = $this->addDevice($user->id, $device_info);
                    $user->accessToken = $token;
                    $user->user_id = $user->id;
                    unset($user->id);
                    unset($user->role_id);
                    unset($user->verification_code);
                    unset($user->remember_token);
                    unset($user->referral_code);
                    unset($user->referred_by);
                    unset($user->referred_count);
                }
                return $this->response->responseSuccess(trans('appster::messages.login_success'), $user);
            } else {
                $user_detail = $this->findUserByEmail($json['email']);
                if (!is_object($user_detail)) {
                    return $this->response->responseUnauthorized(trans('appster::messages.userNotRegistered'));
                }
                return $this->response->responseUnauthorized(trans('appster::messages.credential_mismatched'));
            }
        } catch (\Exception $ex) {
            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * find user by emailID
     */
    public function findUserByEmail($email) {
        return $this->select('name', 'id', 'role_id', 'is_profile_finished', 'email', 'status')->where('email', $email)->first();
    }

    /**
     * 
     * @param function : ChangePassword()
     * @returns Description : Here we are updating password of the user.
     * 
     */
    public function changePassword($user_id, $old_password, $new_password) {

        try {
            $this->response = new ResponseFormatter();
            $check_password = $this->where('id', $user_id)
                    ->where('status', Config::get('services.appster.activeStatus'))
                    ->select('*')
                    ->first();

            if (is_object($check_password)) {
                if (!\Hash::check($old_password, $check_password->password)) {
                    return $this->response->responseUnauthorized(trans('appster::messages.OldPasswordWrong'));
                }
                $update_respnse = $this->updatePassword($user_id, $new_password);
                if ($update_respnse) {
                    return $this->response->responseSuccess(trans('appster::messages.success'));
                }
            }
            return $this->response->responseUnauthorized(trans('appster::messages.unauthorized'));
        } catch (\Exception $ex) {

            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : updatePassword()
     * @returns Description : Here we are updating password.
     * 
     */
    public function updatePassword($user_id, $new_password) {

        $user = $this->find($user_id);
        $user->password = bcrypt($new_password);
        $user->is_temp_pass = Config::get('services.appster.isTempPass');
        $user->save();

        return $user;
    }

    /**
     * 
     * @param function : forgotPassword()
     * @returns Description : Here we are sending duplicate password of the user.
     * 
     */
    public function forgotPassword($email) {
        try {
            $this->response = new ResponseFormatter();
            $utility = new UtilityHelper();

            $temp_password = $utility->generateRandomString(Config::get('services.appster.randomPasswordLength'));

            $user_info = $this->where('email', $email)
                    ->select('*')
                    ->first();


            if (!$user_info) {
                return $this->response->responseUnauthorized(trans('appster::messages.user_not_found_reset_pass'));
            }

            $is_varificaiton_mail_enable = Config::get('services.appster.isVarificaitonMailEnable');
            if ($is_varificaiton_mail_enable && $user->status == Config::get('services.appster.notActive')) {

                if ($user_info['status'] == Config::get('services.appster.activeStatus')) {
                    return $this->response->responseUnauthorized(trans('appster::messages.userNotVerified'));
                }
            }

            $this->updatePassword($user_info["id"], $temp_password);
            $user_data = array('email' => $email, 'name' => $user_info['name']);
            Mail::send('appsterView::email.resetpassword', ['tempPassword' => $temp_password, 'name' => $user_info['name'], 'email' => $email], function($message) use ($user_data) {
                $message->to($user_data['email'], $user_data['name'])->subject('Temporary Password Request');
            });
            return $this->response->responseSuccess(trans('appster::messages.mail_sent'));
        } catch (\Exception $ex) {
            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : socialLogin()
     * @returns Description : Here we are validating facebook user.
     * 
     */
    public function socialLogin($json) {
        try {
            $this->response = new ResponseFormatter();
            $client = new \GuzzleHttp\Client();
            $facebook_token = $json['facebookToken'];

            $res = $client->get('https://graph.facebook.com/v2.8/me?fields=name,picture,email&access_token=' . $facebook_token);

            if ($res->getStatusCode() != 200) {

                return $this->response->responseServerError(trans('appster::messages.facebook_login_fail'));
            }


            $facebook_user = json_decode($res->getBody());
            if ($facebook_user->id != $json['facebookId']) {
                return $this->response->responseServerError(trans('appster::messages.facebook_mismatch'), '11');
            }

            $email = $json['email'];
            $facebook_id = $json['facebookId'];
            $name = $json['fullname'];
            $user_data = array("email" => $email, "facebookId" => $facebook_id, "name" => $name);
            $social_user = $this->socialSignIn($user_data);
            $device_info = $json['deviceInfo'];
            // Store Device Information                
            $token = $this->addDevice($social_user->id, $device_info);
            $social_user->accessToken = $token;
            $social_user->user_id = $social_user->id;
            unset($social_user->id);
            return $this->response->responseSuccess(trans('appster::messages.facebook_login_success'));
        } catch (\Exception $ex) {
            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : socialSignIn()
     * @returns Description : Here we are authenticating user after validating facebook user.
     * 
     */
    public function socialSignIn($user_data) {
        $user = $this->where('facebook_id', $user_data['facebookId'])
                        ->orWhere('email', $user_data['email'])->first();
        if (is_object($user)) {
            $user->facebook_id = $user_data['facebookId'];
            $user->name = $user_data['name'];
            $user->save();
        } else {
            $utility = new UtilityHelper();
            $referral_code = $utility->generateRandomString(Config::get('services.appster.randomReferralCodeLength'));
            $verification_code = time();

            $this->facebook_id = $user_data['facebookId'];
            $this->email = $user_data['email'];
            $this->name = $user_data['name'];
            $this->role_id = Config::get('services.appster.userRole');
            ;
            $this->phone_number = !empty($user_data['phoneNumber']) ? $user_data['phoneNumber'] : '';
            $this->verification_code = $verification_code;
            $this->referral_code = $referral_code;
            $this->status = Config::get('services.appster.activeStatus');
            $this->saveUserInstance();
            $this->socialSignIn($user_data);
        }
        return $user;
    }

    /**
     * 
     * @param function : logout()
     * @returns Description : Here we are throwing user from the system.
     * 
     */
    public function logout($access_token) {
        try {
            $this->response = new ResponseFormatter();
            $user = $this->unRegisterAll($access_token);
            if ($user) {
                return $this->response->responseSuccess(trans('appster::messages.logout'));
            } else {
                return $this->response->responseUnauthorized(trans('appster::messages.unauthorized'));
            }
        } catch (\Exception $ex) {
            return $this->response->responseServerError($ex->getMessage());
        }
    }

    /**
     * 
     * @param function : unRegisterAll()
     * @returns Description : Here we are fetching user .
     * 
     */
    public function unRegisterAll($access_token) {

        $user_device_model = $this->getUserDeviceModel();
        $user_device_model->where('access_token', $access_token)->delete();
        return true;
    }

    /**
     * 
     * @param function : sendEmailVerificationLink()
     * @returns Description : Here we are sending verification link to the user.
     * 
     */
    public function sendEmailVerificationLink($user_data) {
        $verification_link = base64_encode($user_data->verification_code);
        Mail::send('appsterView::email.verificationemail', ['name' => $user_data->name, 'url' => $verification_link, 'email' => $user_data->email], function($message) use ($user_data) {
            $message->to($user_data->email, $user_data->name)->subject('Email Verification Link');
        });
    }

    public function varification($confirmation_code) {
        $user = $this::select('id')->where('verification_code', base64_decode($confirmation_code))->first();
        if (!$user) {
            $message = trans('appster::messages.invi_already_accepted'); //"Invitation Already Accepted";
            $data = ['message' => $message];
        } else {
            $user->status = Config::get('services.appster.activeStatus');
            $user->verification_code = '';
            $user->save();
            $message = trans('appster::messages.invi_accepted'); //"Invitation Accepted";
            $data = ['message' => $message];
        }
        return view('appsterView::verificationpage', $data);
    }

}
