-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 14, 2017 at 09:31 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `appster_library`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_role`
--

CREATE TABLE IF NOT EXISTS `tbl_role` (
`id` int(11) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_role`
--

INSERT INTO `tbl_role` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Admin', NULL, NULL, NULL),
(2, 'Home Owner', NULL, NULL, NULL),
(3, 'Designer', NULL, NULL, NULL),
(4, 'Builder', NULL, NULL, NULL),
(5, 'Assistant', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `phone_number` varchar(255) CHARACTER SET utf8 NOT NULL,
  `profile_pic` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referral_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referred_by` int(10) unsigned DEFAULT NULL,
  `referred_count` int(11) DEFAULT '3',
  `role_id` int(11) unsigned NOT NULL,
  `verification_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `is_profile_finished` tinyint(1) NOT NULL DEFAULT '1',
  `notification_badge` int(11) NOT NULL DEFAULT '0',
  `is_temp_pass` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `name`, `email`, `password`, `facebook_id`, `phone_number`, `profile_pic`, `referral_code`, `referred_by`, `referred_count`, `role_id`, `verification_code`, `status`, `is_profile_finished`, `notification_badge`, `is_temp_pass`, `last_login`, `created_at`, `updated_at`, `deleted_at`, `remember_token`) VALUES
(1, 'Admin', 'admin@appster.in', '$2y$10$T3vh.tI/5Y6OHvP1sLIoF.TJVGYq/32J3TTOKJwaZGTmTSF3jzGi6', '', '', '', '', NULL, NULL, 1, '', 1, 0, 0, '0', '0000-00-00 00:00:00', '2016-10-27 23:09:23', '2016-12-13 21:52:20', NULL, 'P8QNwnof4JSWhsaWyGmue83ZFeg8uK6F0FKDBI1K26qmhb7JHOSrjC9ahl2E'),
(114, 'danish', 'danish@appster.in', '$2y$10$FxruK5EMXW6BnaD8CSUx9ejAIAH7RjBRZUepbWc.XZkpBywSEY4RO', '', '', NULL, NULL, NULL, 3, 2, '1489462359', 1, 1, 0, '0', '2017-03-14 06:59:47', '2017-03-13 22:33:57', '2017-03-14 01:29:47', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_device`
--

CREATE TABLE IF NOT EXISTS `tbl_user_device` (
`id` int(11) NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `unique_device_id` varchar(255) NOT NULL,
  `device_token` varchar(255) NOT NULL,
  `device_type` varchar(255) NOT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1154 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_device`
--

INSERT INTO `tbl_user_device` (`id`, `user_id`, `unique_device_id`, `device_token`, `device_type`, `access_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1095, 45, '1ABCA27D7819493189E53A9E3B63083E', '220a349cb80e4776b123fed62c29641a9807ac8b187feb57f93287b0bde0ae25', 'iOS', '7c9a9eb6a25857ea43093270f1d34912', '2016-12-05 00:49:48', '2016-12-05 00:49:48', NULL),
(1096, 46, 'DDC9EBBF4D64453AB63FCB69D009EE41', '2a8851bc486ea31b8f041a0f68765e5e8595a020e0e3c6eb457edf58aee651dc', 'iOS', '6962ca580ca07cdaa00dbc06d7f2ed52', '2016-12-05 03:51:42', '2016-12-05 03:51:42', NULL),
(1098, 48, '716FDB4D259B4C11B319C5461E85B019', '3f40f570429c95dce37e5fcdc54fa26bfb10e8b538a9e1917a93d675cbe2111d', 'iOS', '995e023f71a23ec74950fde5acea41ed', '2016-12-06 04:43:03', '2016-12-06 04:43:03', NULL),
(1106, 52, '279BCCECF07C473398E2E49D75399B56', 'e894812e3c432255328145f8ee1b4711f011e547c15f8d793a44da797ffbe7af', 'iOS', '14acb4b5a705f2711beff96d508127b1', '2016-12-11 23:24:29', '2016-12-11 23:24:29', NULL),
(1107, 42, 'C4F765FE65DB492E9327D9F9D147A115', '6b941ce25bc18e52ad7c7cc4d89cc9c976a97e602dd8f3b88f5e25dafa449564', 'iOS', '639ad0933d379fe1c42cc91e368952cc', '2016-12-11 23:27:52', '2016-12-11 23:27:52', NULL),
(1113, 54, 'E01C03F1B95B4CCAB4D8A2C4ACCA2628', 'e51e6f0bfa3415dd8fa401aa639a74839f7fea66227a96caa403d32b8c683205', 'iOS', '4d0900f07dc8bf6a9b06bad9abe42734', '2016-12-12 22:13:37', '2016-12-12 22:13:37', NULL),
(1123, 56, '0D8C88411CE946858458E80A24AA23AC', '', 'iOS', '9cf7bc636b9112555f5461bb9f03b284', '2016-12-15 03:56:41', '2016-12-15 03:56:41', NULL),
(1153, 114, '373dd2dds73332fsdf3', '123hdhhebjebejk5itufjfbjfb', 'Android', '6a7842333e4957593d47fbe5c1eb252a', '2017-03-14 01:29:47', '2017-03-14 01:29:48', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_role`
--
ALTER TABLE `tbl_role`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`), ADD KEY `users_referred_by_foreign` (`referred_by`), ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `tbl_user_device`
--
ALTER TABLE `tbl_user_device`
 ADD PRIMARY KEY (`id`), ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_role`
--
ALTER TABLE `tbl_role`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `tbl_user_device`
--
ALTER TABLE `tbl_user_device`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1154;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
